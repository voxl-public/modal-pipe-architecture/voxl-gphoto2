/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <gst/gst.h>
#include <gst/video/video.h>
#include "context.h"

// This is a callback to indicate when the pipeline needs data
void start_feed(GstElement *source, guint size, context_data *data) {
    if (data->frame_debug) printf("*** Start feeding ***\n");
    data->need_data = 1;
}

// This is a callback to indicate when the pipeline no longer needs data.
void stop_feed(GstElement *source, context_data *data) {
    if (data->frame_debug) printf("*** Stop feeding ***\n");
    data->need_data = 0;
}

// Camera file handler size function. This isn't called.
static int input_size(void *user_data, uint64_t *size) {
    printf("input_size called\n");
    return 0;
}

// Camera file handler read function. This isn't called.
static int input_read(void *user_data, unsigned char *data, uint64_t *len) {
    printf("input_read called\n");
    return 0;
}

// Camera file handler write function. This is called when there is a new
// frame available and it needs to be written into the "file"
static int input_write(void *user_data, unsigned char *data, uint64_t *len) {

    context_data *ctx = (context_data*) user_data;
    uint32_t data_len = *((uint32_t*) len);
    GstBuffer *gst_buffer;
    GstMapInfo info;
    GstFlowReturn status;

    if (ctx->frame_debug) printf("input_write called. Length = %u\n", data_len);

    // The need_data flag is set by the pipeline callback asking for
    // more data.
    if (ctx->need_data) {

        // Allocate a gstreamer buffer to hold the frame data
        gst_buffer = gst_buffer_new_and_alloc(data_len);
        gst_buffer_map(gst_buffer, &info, GST_MAP_WRITE);

        // TODO: Is there any way to avoid this copy?
        memcpy(info.data, data, data_len);

        // Setup some reasonable timestamp. This will be replaced by the MPA
        // timestamp once the frame processing has been complete. This is just
        // needed to make the pipeline work.
        GST_BUFFER_TIMESTAMP(gst_buffer) = (guint64) ctx->input_frame_number;
        GST_BUFFER_DURATION(gst_buffer) = gst_util_uint64_scale (ctx->input_frame_number, GST_SECOND, 30);

        // Signal that the frame is ready for use
        g_signal_emit_by_name(ctx->app_source, "push-buffer", gst_buffer, &status);
        if (status == GST_FLOW_OK) {
            if (ctx->frame_debug) printf("Frame %d accepted\n", ctx->input_frame_number);
        } else {
            fprintf(stderr, "ERROR: New frame rejected\n");
        }

        ctx->input_frame_number++;

        // Release the buffer so that we don't have a memory leak
        gst_buffer_unmap(gst_buffer, &info);
        gst_buffer_unref(gst_buffer);
    }

    return 0;
}

// This is the main thread that reads frame data from the pipe and submits
// it into the pipeline for processing.
void *input_thread(void *vargp) {

    int rc = 0;

    context_data *ctx = (context_data*) vargp;

    if (ctx->debug) printf("buffer processing thread starting\n");

    printf("%p\n", (void*) &ctx->file_handler);

    ctx->file_handler.size  = input_size;
    ctx->file_handler.read  = input_read;
    ctx->file_handler.write = input_write;

    rc = gp_file_new_from_handler(&ctx->camera_file, &ctx->file_handler,
                                  ctx);
    if (rc) {
        fprintf(stderr, "ERROR: gp_file_new_from_handler failed: %d %s\n", rc,
                gp_result_as_string(rc));
        return NULL;
    } else {
        if (ctx->debug) printf("Created new frame handler\n");
    }

    // This is the main input processing loop
    while (ctx->running) {

        rc = gp_camera_capture_preview(ctx->camera_ptr, ctx->camera_file,
                                       ctx->gp_context_ptr);
        if (rc) {
            fprintf(stderr, "ERROR: gp_camera_capture_preview failed: %d %s\n",
                    rc, gp_result_as_string(rc));
            ctx->running = 0;
            break;
        } else {
            if (ctx->frame_debug) printf("Preview successful\n");
        }
    }

    if (ctx->debug) printf("buffer processing thread ending\n");

    return NULL;
}
