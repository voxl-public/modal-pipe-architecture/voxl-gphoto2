/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/app/gstappsrc.h>
#include <modal_pipe_server.h>
#include "util.h"
#include "output.h"
#include "context.h"

// This is the name of the pipe directory needed for MPA
#define STREAM_PIPE_DIR (MODAL_PIPE_DEFAULT_BASE_DIR "gphoto2/")

// Initialization function
int output_init(context_data *ctx) {
    // Populate static fields of the metadata
    ctx->frame_data.magic_number = CAMERA_MAGIC_NUMBER;

    // Setup the MPA server channel
	if (pipe_server_init_channel(0, STREAM_PIPE_DIR, 0)) {
        fprintf(stderr, "ERROR: Couldn't initialize MPA channel\n");
        return -1;
    }

    return 0;
}

GstFlowReturn new_sample(GstElement *source, context_data *data) {

    int rc = 0;
    struct timespec time;
    GstCaps *caps = NULL;
    GstSample *sample = NULL;
    GstBuffer *buffer = NULL;

    // Retrieve the buffer
    g_signal_emit_by_name(data->app_sink, "pull-sample", &sample);
    if (sample) {
        if (data->frame_debug) printf("*** Got new sample. ***\n");

        if (data->frame_data_initialized) {
            buffer = gst_sample_get_buffer(sample);
            if (buffer) {
                GstMapInfo info;
                gst_buffer_map(buffer, &info, GST_MAP_READ);
                if (data->frame_debug) printf("*** Data size: %d ***\n", info.size);

                rc = clock_gettime(CLOCK_MONOTONIC, &time);
                if (data->frame_debug) printf("Frame timestamp: %lu seconds, %lu nanoseconds\n", time.tv_sec, time.tv_nsec);

                data->frame_data.timestamp_ns = ((uint64_t) time.tv_sec) * 1000000000;
                data->frame_data.timestamp_ns += time.tv_nsec;

                rc = pipe_server_send_camera_frame_to_channel(0, data->frame_data,
                                                              (char*) info.data);
                if (rc) {
                    fprintf(stderr, "Error: Failed to write meta data and frame bytes to the pipe\n");
                    data->running = 0;
                    gst_buffer_unmap(buffer, &info);
                    gst_sample_unref(sample);
                    return GST_FLOW_ERROR;
                }

                gst_buffer_unmap(buffer, &info);
            } else {
                if (data->frame_debug) fprintf(stderr, "*** sample get buffer failed ***\n");
                gst_sample_unref(sample);
                return GST_FLOW_ERROR;
            }
        } else {
            caps = gst_sample_get_caps(sample);
            if (caps) {
                if (data->debug) print_caps(caps, "voxl-gphoto2: ");

                if (verify_caps_format(caps, "I420")) {
                    if (data->debug) printf("Format is I420\n");
                    data->frame_data.format = IMAGE_FORMAT_YUV420;
                } else {
                    if (data->debug) fprintf(stderr, "ERROR: Format is not I420\n");
                    data->running = 0;
                    gst_sample_unref(sample);
                    return GST_FLOW_ERROR;
                }

                int temp_value;
                if (get_caps_int(caps, "width", &temp_value)) {
                    data->frame_data.width = temp_value;
                    if (data->debug) printf("Frame width: %d\n", data->frame_data.width);
                } else {
                    if (data->debug) fprintf(stderr, "ERROR: Could not get frame width\n");
                    data->running = 0;
                    gst_sample_unref(sample);
                    return GST_FLOW_ERROR;
                }

                if (get_caps_int(caps, "height", &temp_value)) {
                    data->frame_data.height = temp_value;
                    if (data->debug) printf("Frame width: %d\n", data->frame_data.height);
                } else {
                    if (data->debug) fprintf(stderr, "ERROR: Could not get frame height\n");
                    data->running = 0;
                    gst_sample_unref(sample);
                    return GST_FLOW_ERROR;
                }

                data->frame_data.size_bytes = (data->frame_data.width *
                                              (data->frame_data.height / 2) * 3);
                data->frame_data.stride = ((data->frame_data.width / 2) * 3);

                if (pipe_server_set_default_pipe_size(0, data->frame_data.size_bytes)) {
                    fprintf(stderr, "ERROR: Couldn't set MPA pipe size\n");
                    data->running = 0;
                    gst_sample_unref(sample);
                    return GST_FLOW_ERROR;
                }

                data->frame_data_initialized = 1;

            } else {
                if (data->frame_debug) fprintf(stderr, "*** sample get caps failed ***\n");
                data->running = 0;
                gst_sample_unref(sample);
                return GST_FLOW_ERROR;
            }
        }

        gst_sample_unref(sample);
        return GST_FLOW_OK;
    } else {
        if (data->frame_debug) fprintf(stderr, "*** pull-sample failed ***\n");
    }

    return GST_FLOW_ERROR;
}
