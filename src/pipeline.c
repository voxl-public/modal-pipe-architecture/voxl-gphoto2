/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/app/gstappsrc.h>
#include "util.h"
#include "input.h"
#include "output.h"
#include "context.h"

static context_data *context;

static void create_elements(context_data *ctx) {
    ctx->app_source = gst_element_factory_make("appsrc", "frame_source");
    ctx->parser_queue = gst_element_factory_make("queue", "parser_queue");
    ctx->jpeg_parser = gst_element_factory_make("jpegparse", "jpeg_parser");
    ctx->decoder_queue = gst_element_factory_make("queue", "decoder_queue");
    ctx->jpeg_decoder = gst_element_factory_make("jpegdec", "jpeg_decoder");
    ctx->app_sink_queue = gst_element_factory_make("queue", "app_sink_queue");
    ctx->app_sink = gst_element_factory_make("appsink", "frame_sink");
}

static int verify_element_creation(context_data *ctx) {
    if (ctx->app_source) {
        if (ctx->debug) printf("Made app_source\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make app_source\n");
        return -1;
    }
    if (ctx->parser_queue) {
        if (ctx->debug) printf("Made parser_queue\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make parser_queue\n");
        return -1;
    }
    if (ctx->jpeg_parser) {
        if (ctx->debug) printf("Made jpeg_parser\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make jpeg_parser\n");
        return -1;
    }
    if (ctx->decoder_queue) {
        if (ctx->debug) printf("Made decoder_queue\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make decoder_queue\n");
        return -1;
    }
    if (ctx->jpeg_decoder) {
        if (ctx->debug) printf("Made jpeg_decoder\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make jpeg_decoder\n");
        return -1;
    }
    if (ctx->app_sink_queue) {
        if (ctx->debug) printf("Made app_sink_queue\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make app_sink_queue\n");
        return -1;
    }
    if (ctx->app_sink) {
        if (ctx->debug) printf("Made app_sink\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make app_sink\n");
        return -1;
    }

    return 0;
}

// These are the callbacks to let us know of bus messages
static void warn_cb(GstBus *bus, GstMessage *msg, context_data *data) {
    GError *err;
    gchar *debug_info;

    printf("*** Got gstreamer warning callback ***\n");

    /* Print error details on the screen */
    gst_message_parse_error(msg, &err, &debug_info);
    g_printerr("Warning received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message);
    g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
    g_clear_error(&err);
    g_free(debug_info);
}

static void error_cb(GstBus *bus, GstMessage *msg, context_data *data) {
    GError *err;
    gchar *debug_info;

    printf("*** Got gstreamer error callback ***\n");

    /* Print error details on the screen */
    gst_message_parse_error(msg, &err, &debug_info);
    g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src), err->message);
    g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
    g_clear_error(&err);
    g_free(debug_info);
}

int pipeline_init(context_data *ctx) {

    if (ctx->debug) printf("Creating media pipeline\n");

    int rc = 0;

    gboolean success;
    GstBus* bus;

    // Create an empty pipeline
    ctx->pipeline = gst_pipeline_new(NULL);
    if (ctx->pipeline) {
        if (ctx->debug) printf("Made empty pipeline\n");
    } else {
        fprintf(stderr, "ERROR: couldn't make empty pipeline\n");
        return -1;
    }

    // Create all of the needed elements
    create_elements(ctx);

    // Verify that all needed elements were created
    if (verify_element_creation(ctx)) return -1;

    // Configure the application source
    g_object_set(ctx->app_source, "is-live", 1, NULL);

    g_signal_connect(ctx->app_source, "need-data", G_CALLBACK(start_feed), ctx);
    g_signal_connect(ctx->app_source, "enough-data", G_CALLBACK(stop_feed), ctx);

    // Configure the video parser input queue
    g_object_set(ctx->parser_queue, "leaky", 1, NULL);
    g_object_set(ctx->parser_queue, "max-size-buffers", 100, NULL);

    // Configure the jpeg decoder input queue
    g_object_set(ctx->decoder_queue, "leaky", 1, NULL);
    g_object_set(ctx->decoder_queue, "max-size-buffers", 100, NULL);

    // Configure the app sink input queue
    g_object_set(ctx->app_sink_queue, "leaky", 1, NULL);
    g_object_set(ctx->app_sink_queue, "max-size-buffers", 100, NULL);

    // Configure the app sink
    g_signal_connect(ctx->app_sink, "new-sample", G_CALLBACK(new_sample), ctx);
    g_object_set(ctx->app_sink, "emit-signals", TRUE, NULL);

    // Put all needed elements into the pipeline
    gst_bin_add_many(GST_BIN(ctx->pipeline),
                     ctx->app_source,
                     ctx->parser_queue,
                     ctx->jpeg_parser,
                     ctx->decoder_queue,
                     ctx->jpeg_decoder,
                     ctx->app_sink_queue,
                     ctx->app_sink,
                     NULL);

    success = gst_element_link_many(ctx->app_source,
                                    ctx->parser_queue,
                                    ctx->jpeg_parser,
                                    ctx->decoder_queue,
                                    ctx->jpeg_decoder,
                                    ctx->app_sink_queue,
                                    ctx->app_sink,
                                    NULL);
    if ( ! success) fprintf(stderr, "ERROR: couldn't link pipeline\n");

    // Set up our bus and callback for messages
    bus = gst_element_get_bus(ctx->pipeline);
    if (bus) {
        gst_bus_add_signal_watch(bus);
        g_signal_connect(G_OBJECT(bus), "message::warn", (GCallback) warn_cb, ctx);
        g_signal_connect(G_OBJECT(bus), "message::error", (GCallback) error_cb, ctx);
        gst_object_unref(bus);
    } else {
        if (ctx->debug) printf("WARNING: Could not attach error callback to pipeline\n");
    }

    // Start playing the pipeline
    gst_element_set_state(ctx->pipeline, GST_STATE_PLAYING);

    return 0;
}
