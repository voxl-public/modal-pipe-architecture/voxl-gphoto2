/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @file context.h
 *
 * This file contains the definition of the main data structure for the
 * application.
 */

#ifndef CONTEXT_H
#define CONTEXT_H

#include <gphoto2/gphoto2.h>
#include <gphoto2/gphoto2-port-info-list.h>
#include <pthread.h>
#include <modal_pipe.h>

#define MAX_UVC_DEVICE_STRING_LENGTH 64

// Structure to contain all needed information, so we can pass it to callbacks
typedef struct _context_data {

    GPContext         *gp_context_ptr;
    Camera            *camera_ptr;
    CameraFile        *camera_file;
    CameraFileHandler  file_handler;

    GstElement *pipeline;
    GstElement *app_source;
    GstElement *parser_queue;
    GstElement *jpeg_parser;
    GstElement *decoder_queue;
    GstElement *jpeg_decoder;
    GstElement *app_sink_queue;
    GstElement *app_sink;

    uint32_t input_frame_number;

    volatile int            frame_data_initialized;
    camera_image_metadata_t frame_data;

    int debug;
    int frame_debug;
    int libgphoto2_debug;

    volatile int need_data;
    volatile int running;

    pthread_mutex_t lock;

} context_data;

#endif // CONTEXT_H
